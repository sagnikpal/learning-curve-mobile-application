import 'package:flutter/material.dart';
import 'play.dart';
import 'gameLogic.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:async';


class Result extends StatelessWidget {
  final GameWord game;
  final int idx;
  Result.fromData(
      this.game, this.idx
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Result"),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _getResult(),
            Text(
              "Word: " + game.secretWord,
              style: TextStyle(fontSize: 20),
            ),
            FlatButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Next Word",
                  style: TextStyle(fontSize: 40,),
                ),
              ),
              color: Colors.blueGrey,
              onPressed: () {
                game.idx = game.idx+1;
                game.generateRandomWord(idx);
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return Play.fromData(game, game.idx);
                }));
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _getResult() {
    if (game.isWordGuessed()) {
      return Column(
        children: <Widget>[
          Image.asset(
            'images/unnamed.png',
            height: 100,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Text(
              game.desc,
              style: TextStyle(fontSize: 32),
            ),
          ),
        ],
      );
    } else {
      return Center(
        child: Column(
          children: <Widget>[
            Image.asset(
              'images/unnamed.png',
              height: 100,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 65.0, vertical: 20.0),
              child: Text(
                game.desc,
                style: TextStyle(fontSize: 32),
              ),
            ),
          ],
        ),
      );
    }
  }
}

