import 'package:flutter/material.dart';
import 'package:game/storyGameLogic.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'storyresult.dart';

class UnPage extends StatefulWidget {

  StoryGame story;
  UnPage.fromData(this.story);
  @override
  _UnPageState createState() => _UnPageState(story);
}

class _UnPageState extends State<UnPage> {

  StoryGame story;
  bool _speaking = false;
  FlutterTts flutterTts = FlutterTts();
  _UnPageState(this.story);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(story.storyTitle),
      ),
      body: _getWidget(),
      floatingActionButton: FloatingActionButton(
        child: _Icons(),
        onPressed: () {
          _speaker();
        },
        heroTag: "mic",
      ),
    );
  }

  Widget _speaker(){
    if(_speaking == true){
      _stop();
      setState(() {
        _speaking = false;
      });
    }
    else{
      _speak();
      setState(() {
        _speaking = true;
      });
    }
  }

  Widget _getWidget(){
    if(story.presentScene['interactive'] == 1){
      return _getIntWidget();
    }
    else{
      return _getUnIntWidget();
    }
  }

  Widget _getIntWidget(){
    return Container(
//        color: Colors.blue,
      margin: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(12.0),
              height: 380.0,
              width: 400.0,
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.2),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: Image.memory(story.bytes),
              )
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0),
            padding: EdgeInsets.all(15.0,),
//              height: 380.0,
            width: 600.0,
            decoration: BoxDecoration(
                color: Colors.blueGrey.withOpacity(0.6),
                borderRadius: BorderRadius.all(Radius.circular(15.0))
            ),
            child: Text(story.description, style: TextStyle(fontSize: 20.0),),
          ),
          Expanded(
            child: new ListView.builder
              (
                itemCount: story.options.length,
                itemBuilder: (BuildContext ctxt, int Index) {
                  return RaisedButton(child: new Text(story.options[Index]['desc']), onPressed: (){});
                }
            ),
          ),
          RaisedButton(
            child: Text('Next Scene'),
            onPressed: (){
              if(story.sceneIdx == story.storyData['scenes'].length - 1){
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return StoryResult.fromData(story);
                }));
              }
              setState(() {
                story.nextScene();
                story.LoadStory();
                _speaking = false;
              });
            },
          ),
        ],
      ),
    );
  }

  Widget _getUnIntWidget(){
    return Container(
//        color: Colors.blue,
      margin: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(12.0),
              height: 385.0,
              width: 400.0,
              decoration: BoxDecoration(
                  color: Colors.blueGrey.withOpacity(0.2),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: Image.memory(story.bytes),
              )
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 20.0),
            padding: EdgeInsets.all(15.0,),
//              height: 380.0,
            width: 600.0,
            height: 250.0,
            decoration: BoxDecoration(
                color: Colors.blueGrey.withOpacity(0.6),
                borderRadius: BorderRadius.all(Radius.circular(15.0))
            ),
            child: Text(story.description, style: TextStyle(fontSize: 20.0),),
          ),
          RaisedButton(
            child: Text('Next Scene'),
            onPressed: (){
              if(story.sceneIdx == story.storyData['scenes'].length - 1){
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return StoryResult.fromData(story);
                }));
              }
              setState(() {
                story.nextScene();
                story.LoadStory();
                _speaking = false;
              });
            },
          )
        ],
      ),
    );
  }

  Widget _Icons() {
    if(_speaking == true){
      return Icon(Icons.mic);
    }
    else{
      return Icon(Icons.mic_off);
    }
  }

  void _speak() async {
    await flutterTts.setSpeechRate(0.8);
    await flutterTts.speak(story.description);
  }

  void _stop() async {
    await flutterTts.stop();
  }
}


